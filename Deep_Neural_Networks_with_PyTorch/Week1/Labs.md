<img src="images/IDSNlogo.png" width = "300">

# Labs

You can experiment with PyTorch in the labs, by clicking the open tool section . As shown in the following image:

<img src="images/1.png" width = "600">

Sometimes the lab will not open and you will see the following message:

<img src="images/2.png" width = "300">

If this is the case you should logout as shown in the image , wait 2 minutes and try again.

<img src="images/3.png" width = "600">

## Author(s)
[Joseph Santarcangelo](https://www.linkedin.com/in/joseph-s-50398b136/)

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-17 | 2.0 | Shubham | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |