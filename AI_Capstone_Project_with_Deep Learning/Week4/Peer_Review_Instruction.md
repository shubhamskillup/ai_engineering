<img src="images/IDSNlogo.png" width = "300">

# Peer Review Instruction

You will need to load the notebook for the next section, copy the URL, **remember in this lab you do not need to transform the dataset** :

**Jupyter Notebook:**

https://cocl.us/DL_capstone_coursera_PyTorch




## Author(s)
[Joseph Santarcangelo](https://www.linkedin.com/in/joseph-s-50398b136/)

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-17 | 2.0 | Shubham | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |